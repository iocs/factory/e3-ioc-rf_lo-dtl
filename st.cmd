
require essioc
require iocmetadata
require lobox

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("DEVICENAME", "DTL-030:RFS-LO-010")
epicsEnvSet("IPADDR", "dtl3-lob-009.tn.esss.lu.se")

iocshLoad("$(lobox_DIR)/lobox.iocsh")

pvlistFromInfo("ARCHIVE_THIS", "$(DEVICENAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(DEVICENAME):SavResList")

